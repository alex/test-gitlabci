# prds

## Raspberry Pi 3 Model B+

### Hardware Info[^1]

- SoC: `BCM2837b0`
- Memory: `1GB`
- Connectivity:
  - HDMI
  - 4x USB 2.0
  - standard 15-pin, 1.0mm pitch, 16mm width, CSI (camera) port
  - standard 15-pin, 1.0mm pitch, 16mm width, DSI (display) port
  - 3.5mm AV jack
  - 300Mb/s Ethernet RJ45 with PoE support
  - 2.4/5GHz dual-band 802.11ac Wi-Fi (100Mb/s)
  - Bluetooth 4.2, Bluetooth Low Energy (BLE)
  - microSD card slot
  - micro USB power

### Pinout[^2]

| Connected | Value | Pin | Pin | Value | Connected |
| - | -: | :-: | :-: | :- | - |
| - | 3V3 | **1** | **2** | 5V | - |
| - | GPIO2 | **3** | **4** | 5V | - |
| - | GPIO3 | **5** | **6** | GND | - |
| - | GPIO4 | **7** | **8** | GPIO14 | - |
| - | GND | **9** | **10** | GPIO15 | - |
| - | GPIO17 | **11** | **12** | GPIO18 | - |
| - | GPIO27 | **13** | **14** | GND | - |
| - | GPIO22 | **15** | **16** | GPIO23 | - |
| - | 3V3 | **17** | **18** | GPIO24 | - |
| - | GPIO10 | **19** | **20** | GND | - |
| - | GPIO9 | **21** | **22** | GPIO25 | - |
| - | GPIO11 | **23** | **24** | GPIO8 | - |
| - | GND | **25** | **26** | GPIO7 | - |
| - | GPIO0 | **27** | **28** | GPIO1 | - |
| - | GPIO5 | **29** | **30** | GND | - |
| - | GPIO6 | **31** | **32** | GPIO12 | - |
| - | GPIO13 | **33** | **34** | GND | - |
| - | GPIO19 | **35** | **36** | GPIO16 | - |
| - | GPIO26 | **37** | **38** | GPIO20 | - |
| - | GND | **39** | **40** | GPIO21 | - |

[^1]: https://www.raspberrypi.com/documentation/computers/raspberry-pi.html

[^2]: https://pinout.xyz/
